**This is the home page of [this documentation](http://wonderland.eode.studio/docs/modular-_introduction.html)**

## Introduction

### Version
> not maintained

### Dependencies
- `Common`

Modular is a small plugin that solves the issue of order of initialization and shutdown of multiple modules with dependencies.

## Example

```C#
using EODE.Wonderland;

// Obligatory Dependency, will be initialized before MyModule
[RequireModule(typeof(MyOtherModule1), typeof(MyOtherModule2))]
// Obligatory Dependency, if they exist, will be initialize before this one
[RequireOptionnalModule(typeof(MyOtherModule3))]
public class MyModule : ModuleBehaviour {
    // The module's init Task (use it to load stuff, make web requests etc...)
    public override async Task InitModule() {
        // Do stuff
        await Task.Delay(1000);
        return;
    }

    public override void ShutdownModule(bool isError) {
        // Clean up
    }
}
```